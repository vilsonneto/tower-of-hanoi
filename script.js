let inicio = document.getElementById('inicio')
let meio = document.getElementById('meio')
let fim = document.getElementById('fim')



let selecionado = ""
let destino = ""
let taSelecionado = false;

let pilhas = document.querySelectorAll(".container")

// função que habilita o jogo
const theGame = (event) => {
    
    const pilha = event.currentTarget;
    
    if ( taSelecionado === false ) {
        
        selecionado = pilha.querySelector(".disco-container")
        pilha.classList.add("selecionado")
        
        taSelecionado = true;
        destino = ""
    } else {

        destino = pilha.querySelector(".disco-container")

        let arrSelecionado = tratamentoDeArray(selecionado)
        let arrDestino = tratamentoDeArray(destino)

        let podeJogar = podeMudar( arrSelecionado, arrDestino )

        if ( podeJogar === true ) {
            mudarPosicao(selecionado, destino)
        }

        
        let elementClass = document.querySelector(".selecionado")
        elementClass.classList.remove("selecionado")
        
        selecionado = ""
        taSelecionado = false;
        arrDestino = tratamentoDeArray(destino)

        let meioArr = tratamentoDeArray(pilhas[1])
        let fimArr = tratamentoDeArray(pilhas[2])
        

        if (verificaVitoria(meioArr) === true || verificaVitoria(fimArr) === true ) {
            console.log(inicio)
            const winner = document.getElementById('winner')
            winner.classList.remove('hidden')
            console.log(arrDestino)
            window.scrollTo(0, 0);
        }
    }
    

}

for (let i = 0; i < pilhas.length; i++) {
    pilhas[i].addEventListener('click', theGame )
}

// função para realizar a alteração
const mudarPosicao = (selecionado, destino) => {
    destino.appendChild( selecionado.lastElementChild )
}



// função de verificação para averiguar se pode realizar a jogada
const podeMudar = (selecionado, destino) => {
    let item = selecionado[selecionado.length - 1]; 
	let lastItemDestino = destino[destino.length - 1] 
    
    if ( item !== undefined ) {
        
        if ( item > lastItemDestino || lastItemDestino === undefined ) {
            return true
		}
        
	}
    
	return false
}

//Tratamento de array vindo do data-num.
function tratamentoDeArray(value ){
    const arr = value.getElementsByClassName('disco')
    let arr2 = []
    for ( let i = 0; i < arr.length; i++ ) {
        arr2.push( arr[i].dataset["num"] )
    }
    let output = arr2.map(Number)
    return output
}

//Verificação / Comparação de igualdade dos arrays, retornando buleano.
function verificaVitoria(valorArr){
    const teste = [1, 2, 3, 4]
    if( valorArr.join() === teste.join() ){
        return true
    }
    return false
}
